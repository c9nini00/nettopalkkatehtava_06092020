<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nettopalkka</title>
</head>
<body>
    <h3>Nettopalkka</h3>
<?php
$palkka = filter_input(INPUT_POST,'palkka',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
$ennakonpid = filter_input(INPUT_POST,'ennakonpid',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
$tyoelakem = filter_input(INPUT_POST,'tyoelakem',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
$tyottomvak = filter_input(INPUT_POST,'tyottomvak',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);


$pid = $palkka / 100 * $ennakonpid;
$elake = $palkka / 100 * $tyoelakem;
$vakuutus = $palkka / 100 * $tyottomvak;
$maksut = $pid+$elake+$vakuutus;

$netto = $palkka - $maksut;

printf("<p>Nettopalkka on %.2f €</p>",$netto);
?>
<a href="index.html">Laske uudestaan</>    
</body>
</html>